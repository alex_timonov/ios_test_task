//
//  RestaurantCell.swift
//  Test task
//
//  Created by Alex Timonov on 5/17/19.
//  Copyright © 2019 Alex Timonov. All rights reserved.
//

import UIKit
import SDWebImage

class RestaurantCell : UITableViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var foodName: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var btnDetails: UIButton!
    
    func setRestaurant(restaurant : Restaurant) {
        foodName.text = restaurant.foodName
        price.text = "$\(restaurant.price)"
        img.sd_imageIndicator = SDWebImageActivityIndicator.gray
        img.sd_setImage(with: URL(string: restaurant.imageUrl))
    }
}
