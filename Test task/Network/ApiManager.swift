//
//  ApiManager.swift
//  Test task
//
//  Created by Alex Timonov on 5/17/19.
//  Copyright © 2019 Alex Timonov. All rights reserved.
//

import Foundation

class ApiManager: NSObject {
    
    static let instance = ApiManager()
    private override init() {}
    
    func getRestaurants(completion: @escaping (Bool, [Restaurant]) -> ()) {
        let restaurantsEndpoint = URL(string: "https://gist.githubusercontent.com/gonchs/b657e6043e17482cae77a633d78f8e83/raw/7654c0db94a3f430888fac0caac675c7e811030a/test_data.json")
        URLSession.shared.dataTask(with: restaurantsEndpoint!){(data, response, error) in
            
            do {
                let restaurants = try JSONDecoder().decode([Restaurant].self, from: data!)
                completion(true, restaurants)
            } catch {
                completion(false, [])
            }
        }.resume()
    }
}
