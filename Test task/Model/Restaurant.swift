//
//  Restaurant.swift
//  Test task
//
//  Created by Alex Timonov on 5/17/19.
//  Copyright © 2019 Alex Timonov. All rights reserved.
//

struct Restaurant: Decodable {
    
    var restaurant: String = ""
    var foodName: String = ""
    var price: Float = 0.0
    var imageUrl: String = ""
    
    enum CodingKeys: String, CodingKey {
        case restaurant
        case foodName = "food_name"
        case price
        case imageUrl = "image_url"
    }
}
