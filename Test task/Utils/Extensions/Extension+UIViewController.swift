//
//  Extension+UIViewController.swift
//  Test task
//
//  Created by Alex Timonov on 5/21/19.
//  Copyright © 2019 Alex Timonov. All rights reserved.
//

import UIKit

@objc extension UIViewController {
    
    func showAlert(alertTitle: String, message: String) {
        let alert = UIAlertController(title: alertTitle, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
