//
//  MainViewModel.swift
//  Test task
//
//  Created by Alex Timonov on 5/21/19.
//  Copyright © 2019 Alex Timonov. All rights reserved.
//

import UIKit

class  MainViewModel: NSObject {
    
    var restaurants : Array<Restaurant> = []
    var showAlertClosure: (()->())?
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    var downloadSuccess: (()->())?
    
    func downloadRestaurants() {
        ApiManager.instance.getRestaurants() {(success, restaurants) in
            if(success) {
                self.restaurants = restaurants
                self.downloadSuccess?()
            } else {
                self.alertMessage = "Error loading restaurants."
            }
        }
    }
    
    func showRestaurantDetails(fromVC: UIViewController, dismisSelector: Selector?) {
        guard let detailsVC = fromVC.storyboard?.instantiateViewController(withIdentifier: "detailsVC") else {
            print("Controller not available")
            return
        }
        let navVC = UINavigationController(rootViewController:detailsVC)
        navVC.modalPresentationStyle = .overCurrentContext
        navVC.navigationBar.topItem?.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: fromVC, action: dismisSelector)
        navVC.navigationBar.topItem?.title = "Detail screen"
        fromVC.present(navVC, animated: true, completion: nil)
    }
}
