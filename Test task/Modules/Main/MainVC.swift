//
//  MainVC.swift
//  Test task
//
//  Created by Alex Timonov on 5/17/19.
//  Copyright © 2019 Alex Timonov. All rights reserved.
//

import UIKit

class MainVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var table: UITableView!
    
    let viewModel = MainViewModel()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        initViewModel()
        viewModel.downloadRestaurants()
    }
    
    func initViewModel() {
        viewModel.downloadSuccess = { [weak self] in
            self?.bindModel()
        }
        viewModel.showAlertClosure = { [weak self] in
            self?.showAlert(alertTitle: "Error", message: (self?.viewModel.alertMessage)!)
        }
    }
    
    private func bindModel() {
        DispatchQueue.main.async() {
            self.table.dataSource = self
            self.table.delegate = self
            self.table.reloadData()
        }
    }
    
    private func bindCell(cell: RestaurantCell, index: Int) {
        cell.setRestaurant(restaurant: viewModel.restaurants[index])
        cell.btnDetails.addTarget(self, action: #selector(showRestaurantDetails),for: UIControl.Event.touchUpInside)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.restaurants.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurantCell") as! RestaurantCell
        bindCell(cell: cell, index: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.showRestaurantDetails()
    }
 

    @objc func hideDetailsVC() {
        self.dismiss(animated: true)
    }
    
    @objc func showRestaurantDetails() {
        self.viewModel.showRestaurantDetails(fromVC: self, dismisSelector: #selector(hideDetailsVC))
    }
}

